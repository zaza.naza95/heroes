# Heroes

Project that lists Marvel heroes.

[![Build Status](https://img.shields.io/appveyor/ci/thiagoloureiro/netcore-jwt-integrator-extension/master.svg)](https://ci.appveyor.com/project/thiagoloureiro/netcore-jwt-integrator-extension)

![](https://img.shields.io/appveyor/tests/thiagoloureiro/dapper-crud-extension.svg)

## Getting Started

clone this repository: `git clone https://gitlab.com/zaza.naza95/heroes.git`

### Prerequisites

1 - Node.js 10.9.0 or later

2 - Npm package manager

### Installing

1 - `npm install`

### Run the Server

1 - `npm start`

### Run the Tests

1 - `npm test`

## Built With

- [ReactJs](https://pt-br.reactjs.org) - The framework used

## Authors

- _Maria Lisboa_ - [MariaLisboa1](https://github.com/MariaLisboa1)
