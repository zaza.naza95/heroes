import React, { useEffect } from 'react';

import { useSelector } from 'react-redux';
import { Container, Detail, ImageThumbnail, HeroDetail } from './styles';

import history from '../../services/history';

export default function HeroDetails() {
  const hero = useSelector((state) => state.hero);

  useEffect(() => {
    if (hero.length > 0) return;
    history.push('/');
  }, [hero]);

  return (
    <Container>
      {hero.map((detail) => (
        <Detail key={detail.id}>
          <ImageThumbnail>
            <img
              src={`${detail.thumbnail.path}.${detail.thumbnail.extension}`}
              alt=""
            />
            <h1 data-testid="hero-name">{detail.name}</h1>
          </ImageThumbnail>

          <HeroDetail>
            <h1>Descrição</h1>
            <p data-testid="hero-description">{detail.description}</p>

            <h1>Series</h1>
            {detail.series.items.map((serie) => (
              <p data-testid="series-name" key={serie.resourceURI}>
                {' '}
                {serie.name}
              </p>
            ))}
          </HeroDetail>
        </Detail>
      ))}
    </Container>
  );
}
